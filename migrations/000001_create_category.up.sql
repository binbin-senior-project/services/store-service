CREATE TABLE IF NOT EXISTS categories(
  id VARCHAR(36) PRIMARY KEY,
  category_name VARCHAR(255) NOT NULL
);