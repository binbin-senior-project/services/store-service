CREATE TYPE status AS ENUM ('active', 'banned');

CREATE TABLE IF NOT EXISTS stores(
  id VARCHAR(36) PRIMARY KEY,
  store_logo VARCHAR (255) NOT NULL,
  store_name VARCHAR (255) NOT NULL,
  store_tagline VARCHAR (255) NOT NULL,
  store_phone VARCHAR (10) NOT NULL,
  store_latitude REAL NOT NULL,
  store_longitude REAL NOT NULL,
  store_status status DEFAULT 'active',
  user_id VARCHAR (36) NOT NULL,
  category_id VARCHAR (36) NOT NULL,
  created_at TIMESTAMP default CURRENT_TIMESTAMP NOT NULL
);