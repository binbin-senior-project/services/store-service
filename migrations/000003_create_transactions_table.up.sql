CREATE TABLE IF NOT EXISTS transactions(
  id serial NOT NULL PRIMARY KEY,
  store_id varchar(36) NOT NULL,
  transaction_description VARCHAR(255) NOT NULL,
  transaction_created_at TIMESTAMP default CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (store_id) REFERENCES stores(id)
);
