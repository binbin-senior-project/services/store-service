package amqp

import (
	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
	h "gitlab.com/mangbinbin/services/store-service/service/helper"
	r "gitlab.com/mangbinbin/services/store-service/service/repository"
)

// AMQPSubscribe
type AMQPSubscribe struct {
	repo    r.RegisterRepository
	channel *amqp.Channel
}

// NewAMQPSubscribe method
func NewAMQPSubscribe(repo r.RegisterRepository, channel *amqp.Channel) {
	subscriber := AMQPSubscribe{
		repo:    repo,
		channel: channel,
	}

	go subscriber.SubscribeTransactionCreatedEvent()
}

// SubscribeTransactionCreatedEvent method
func (d *AMQPSubscribe) SubscribeTransactionCreatedEvent() {
	msgs, err := d.channel.Consume(
		"create.transaction@store.queue", // queue
		"",                               // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)

	failOnError(err, "Failed to register a SubscribeNewMessagingEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			trans := dm.StoreTransactionCreatedEvent{}
			err = h.DeserializePayload(&trans, msg.Body)

			transaction := dm.Transaction{
				StoreID:     trans.StoreID,
				Description: trans.Description,
			}

			d.repo.Transaction.Store(transaction)
		}
	}()

	<-forever
}
