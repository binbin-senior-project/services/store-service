package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/store-service/service/delivery/grpc/proto"
)

// GetNewStoreToday method
func (d *StoreDelivery) GetNewStoreToday(ctx context.Context, req *pb.GetNewStoreTodayRequest) (*pb.GetNewStoreTodayReply, error) {
	// Create Account
	count, err := d.usecase.Analytic.GetNewStoreToday()

	if err != nil {
		return &pb.GetNewStoreTodayReply{}, err
	}

	return &pb.GetNewStoreTodayReply{
		Count: int32(count),
	}, nil
}
