package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/store-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
)

// CreateStoreCategory method
func (d *StoreDelivery) CreateStoreCategory(ctx context.Context, req *pb.CreateStoreCategoryRequest) (*pb.CreateStoreCategoryReply, error) {
	// Create Account
	err := d.usecase.Category.Store(dm.Category{Name: req.Name})

	if err != nil {
		return &pb.CreateStoreCategoryReply{Success: false}, err
	}

	return &pb.CreateStoreCategoryReply{Success: true}, nil
}

// GetStoreCategories method
func (d *StoreDelivery) GetStoreCategories(ctx context.Context, req *pb.GetStoreCategoriesRequest) (*pb.GetStoreCategoriesReply, error) {
	// Create Account
	categorys, err := d.usecase.Category.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetStoreCategoriesReply{}, err
	}

	var categoryReplies = []*pb.StoreCategory{}

	for _, category := range categorys {
		categoryReplies = append(categoryReplies, &pb.StoreCategory{
			Id:   category.ID,
			Name: category.Name,
		})
	}

	rsp := &pb.GetStoreCategoriesReply{
		Categories: categoryReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetStoreCategory method
func (d *StoreDelivery) GetStoreCategory(ctx context.Context, req *pb.GetStoreCategoryRequest) (*pb.GetStoreCategoryReply, error) {
	// Create Account
	category, err := d.usecase.Category.GetByID(req.CategoryID)

	if err != nil {
		return &pb.GetStoreCategoryReply{}, err
	}

	// No error in this case
	return &pb.GetStoreCategoryReply{
		Category: &pb.StoreCategory{
			Id:   category.ID,
			Name: category.Name,
		},
	}, nil
}

// UpdateStoreCategory method
func (d *StoreDelivery) UpdateStoreCategory(ctx context.Context, req *pb.UpdateStoreCategoryRequest) (*pb.UpdateStoreCategoryReply, error) {
	// Create Account
	err := d.usecase.Category.Update(dm.Category{ID: req.CategoryID, Name: req.Name})

	if err != nil {
		return &pb.UpdateStoreCategoryReply{Success: false}, err
	}

	// No error in this case
	return &pb.UpdateStoreCategoryReply{Success: true}, nil
}

// DeleteStoreCategory method
func (d *StoreDelivery) DeleteStoreCategory(ctx context.Context, req *pb.DeleteStoreCategoryRequest) (*pb.DeleteStoreCategoryReply, error) {

	err := d.usecase.Category.Delete(req.CategoryID)

	if err != nil {
		return &pb.DeleteStoreCategoryReply{Success: false}, err
	}

	// No error in this case
	return &pb.DeleteStoreCategoryReply{Success: true}, nil
}
