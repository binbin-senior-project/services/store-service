package grpc

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/mangbinbin/services/store-service/service/delivery/grpc/proto"
	u "gitlab.com/mangbinbin/services/store-service/service/usecase"
	"google.golang.org/grpc"
)

// StoreDelivery Holayy!
type StoreDelivery struct {
	usecase u.RegisterUsecase
}

// NewStoreDelivery method is an intial grpc package
// to start gRPC transportation
func NewStoreDelivery(usecase u.RegisterUsecase) {
	// Register protobuf handler with StoregRPCDelievery struct
	delivery := &StoreDelivery{
		usecase: usecase,
	}

	port := os.Getenv("SERVICE_PORT")

	// Listen out
	log.Println("Listen on port: " + port)

	// Create TCP Connection
	lis, err := net.Listen("tcp", ":"+port)

	// Detect connection error
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create GRPC service
	s := grpc.NewServer()

	// Register GRPC to protobuffer
	pb.RegisterStoreServiceServer(s, delivery)

	// Register TCP connection to GRPC
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
