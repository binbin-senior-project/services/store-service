package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/store-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
)

// CreateStore method
func (d *StoreDelivery) CreateStore(ctx context.Context, req *pb.CreateStoreRequest) (*pb.CreateStoreReply, error) {

	store := dm.Store{
		Logo:       req.Logo,
		Name:       req.Name,
		Tagline:    req.Tagline,
		Phone:      req.Phone,
		Latitude:   req.Latitude,
		Longitude:  req.Longitude,
		UserID:     req.UserID,
		CategoryID: req.CategoryID,
	}

	// Create Account
	err := d.usecase.Store.Store(store)

	if err != nil {
		return &pb.CreateStoreReply{Success: false}, err
	}

	// No error in this case
	return &pb.CreateStoreReply{Success: true}, nil
}

// UpdateStore method
func (d *StoreDelivery) UpdateStore(ctx context.Context, req *pb.UpdateStoreRequest) (*pb.UpdateStoreReply, error) {

	store := dm.Store{
		ID:         req.StoreID,
		Logo:       req.Logo,
		Name:       req.Name,
		Tagline:    req.Tagline,
		Phone:      req.Phone,
		Latitude:   req.Latitude,
		Longitude:  req.Longitude,
		UserID:     req.UserID,
		CategoryID: req.CategoryID,
	}

	err := d.usecase.Store.Update(store)

	if err != nil {
		return &pb.UpdateStoreReply{Success: false}, err
	}

	return &pb.UpdateStoreReply{Success: true}, nil
}

// GetStores method
func (d *StoreDelivery) GetStores(ctx context.Context, req *pb.GetStoresRequest) (*pb.GetStoresReply, error) {
	// Create Account
	stores, err := d.usecase.Store.Fetch(req.Offset, req.Limit)

	if err != nil {
		return &pb.GetStoresReply{}, err
	}

	var storeReplies = []*pb.Store{}

	for _, store := range stores {
		storeReplies = append(storeReplies, &pb.Store{
			Id:         store.ID,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
			Status:     store.Status,
		})
	}

	rsp := &pb.GetStoresReply{
		Stores: storeReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetStore method
func (d *StoreDelivery) GetStore(ctx context.Context, req *pb.GetStoreRequest) (*pb.GetStoreReply, error) {

	store, err := d.usecase.Store.GetByID(req.StoreID)

	if err != nil {
		return &pb.GetStoreReply{}, err
	}

	rsp := &pb.Store{
		Id:         store.ID,
		Logo:       store.Logo,
		Name:       store.Name,
		Tagline:    store.Tagline,
		Phone:      store.Phone,
		Latitude:   store.Latitude,
		Longitude:  store.Longitude,
		CategoryID: store.CategoryID,
		Status:     store.Status,
	}

	// No error in this case
	return &pb.GetStoreReply{Store: rsp}, nil
}

// GetStoresByCategory method
func (d *StoreDelivery) GetStoresByCategory(ctx context.Context, req *pb.GetStoresByCategoryRequest) (*pb.GetStoresByCategoryReply, error) {
	// Create Account
	stores, err := d.usecase.Store.GetByCategoryID(req.CategoryID, req.Offset, req.Limit)

	if err != nil {
		return &pb.GetStoresByCategoryReply{}, err
	}

	var storeReplies = []*pb.Store{}

	for _, store := range stores {
		storeReplies = append(storeReplies, &pb.Store{
			Id:         store.ID,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
			Status:     store.Status,
		})
	}

	// No error in this case
	return &pb.GetStoresByCategoryReply{
		Stores: storeReplies,
	}, nil
}

// GetStoresByKeyword method
func (d *StoreDelivery) GetStoresByKeyword(ctx context.Context, req *pb.GetStoresByKeywordRequest) (*pb.GetStoresByKeywordReply, error) {
	// Create Account
	stores, err := d.usecase.Store.GetByKeyword(req.Keyword, req.Offset, req.Limit)

	if err != nil {
		return &pb.GetStoresByKeywordReply{}, err
	}

	var storeReplies = []*pb.Store{}

	for _, store := range stores {
		storeReplies = append(storeReplies, &pb.Store{
			Id:         store.ID,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
			Status:     store.Status,
		})
	}

	// No error in this case
	return &pb.GetStoresByKeywordReply{
		Stores: storeReplies,
	}, nil
}

// GetStoresByUser method
func (d *StoreDelivery) GetStoresByUser(ctx context.Context, req *pb.GetStoresByUserRequest) (*pb.GetStoresByUserReply, error) {
	// Create Account
	stores, err := d.usecase.Store.GetByUserID(req.UserID, req.Offset, req.Limit)

	if err != nil {
		return &pb.GetStoresByUserReply{}, err
	}

	var storeReplies = []*pb.Store{}

	for _, store := range stores {
		storeReplies = append(storeReplies, &pb.Store{
			Id:         store.ID,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
			Status:     store.Status,
		})
	}

	// No error in this case
	return &pb.GetStoresByUserReply{
		Stores: storeReplies,
	}, nil
}

// ActivateStore method
func (d *StoreDelivery) ActivateStore(ctx context.Context, req *pb.ActivateStoreRequest) (*pb.ActivateStoreReply, error) {
	// Create Account
	err := d.usecase.Store.Activate(req.StoreID)

	if err != nil {
		return &pb.ActivateStoreReply{Success: false}, err
	}

	return &pb.ActivateStoreReply{Success: true}, nil
}

// BanStore method
func (d *StoreDelivery) BanStore(ctx context.Context, req *pb.BanStoreRequest) (*pb.BanStoreReply, error) {
	// Create Account
	err := d.usecase.Store.Ban(req.StoreID)

	if err != nil {
		return &pb.BanStoreReply{Success: false}, err
	}

	return &pb.BanStoreReply{Success: true}, nil
}
