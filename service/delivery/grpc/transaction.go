package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/store-service/service/delivery/grpc/proto"
)

// GetStoreTransactions method
func (d *StoreDelivery) GetStoreTransactions(ctx context.Context, req *pb.GetStoreTransactionsRequest) (*pb.GetStoreTransactionsReply, error) {
	transactions, err := d.usecase.Transaction.GetByStoreID(req.StoreID, int(req.Offset), int(req.Limit))

	var rspTrasctions []*pb.StoreTransaction

	if err != nil {
		return &pb.GetStoreTransactionsReply{}, err
	}

	for _, trans := range transactions {
		rspTrasctions = append(rspTrasctions, &pb.StoreTransaction{
			Id:          trans.ID,
			Description: trans.Description,
			CreatedAt:   trans.CreatedAt.String(),
		})
	}

	return &pb.GetStoreTransactionsReply{
		Transactions: rspTrasctions,
	}, nil
}

// GetStoreTransaction method
func (d *StoreDelivery) GetStoreTransaction(ctx context.Context, req *pb.GetStoreTransactionRequest) (*pb.GetStoreTransactionReply, error) {
	trans, err := d.usecase.Transaction.GetByID(req.TransactionID)

	if err != nil {
		return &pb.GetStoreTransactionReply{}, err
	}

	rspTransaction := &pb.StoreTransaction{
		Id:          trans.ID,
		Description: trans.Description,
		CreatedAt:   trans.CreatedAt.String(),
	}

	return &pb.GetStoreTransactionReply{
		Transaction: rspTransaction,
	}, nil
}
