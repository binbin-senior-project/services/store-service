package domain

import "time"

// Category struct
type Category struct {
	ID   string
	Name string
}

// Store struct
type Store struct {
	ID         string
	Logo       string
	Name       string
	Tagline    string
	Phone      string
	Latitude   float64
	Longitude  float64
	Status     string
	UserID     string
	CategoryID string
	CreatedAt  time.Time
}

// Transaction struct
type Transaction struct {
	ID          string
	StoreID     string
	Description string
	CreatedAt   time.Time
}
