package domain

// StoreTransactionCreatedEvent struct
type StoreTransactionCreatedEvent struct {
	StoreID     string
	Description string
}

// NotificationCreatedEvent struct
type NotificationCreatedEvent struct {
	UserID string
	Title  string
	Body   string
}
