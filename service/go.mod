module gitlab.com/mangbinbin/services/store-service/service

go 1.13

require (
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/golang/protobuf v1.4.0
	github.com/google/uuid v1.1.1
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.4.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	google.golang.org/grpc v1.29.1
	mellium.im/sasl v0.2.1 // indirect
)
