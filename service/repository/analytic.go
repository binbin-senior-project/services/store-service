package repository

import (
	"database/sql"
)

// IStoreRepository interface
type IAnalyticRepository interface {
	GetNewStoreToday() (int, error)
}

// StoreRepository is an object
type AnalyticRepository struct {
	db *sql.DB
}

// NewStoreRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewAnalyticRepository(db *sql.DB) IAnalyticRepository {
	return &AnalyticRepository{
		db: db,
	}
}

// GetNewStoreToday Method
func (r *AnalyticRepository) GetNewStoreToday() (int, error) {
	query := `SELECT COUNT(*) FROM stores WHERE DATE("created_at") = current_date`

	row := r.db.QueryRow(query)

	var count int

	err := row.Scan(
		&count,
	)

	if err != nil {
		return 0, err
	}

	return count, nil
}
