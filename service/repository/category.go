package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
)

// ICategoryRepository interface
type ICategoryRepository interface {
	Store(category dm.Category) error
	Fetch(offset int, limit int) ([]dm.Category, error)
	GetByID(id string) (dm.Category, error)
	Update(storeType dm.Category) error
	Delete(id string) error
}

// CategoryRepository is an object
type CategoryRepository struct {
	db *sql.DB
}

// NewCategoryRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewCategoryRepository(db *sql.DB) ICategoryRepository {
	return &CategoryRepository{
		db: db,
	}
}

// Store Method
func (r *CategoryRepository) Store(category dm.Category) error {
	query := `
		INSERT INTO categories (id, category_name) VALUES ($1, $2)
	`

	category.ID = uuid.New().String()

	_, err := r.db.Query(query, category.ID, category.Name)

	return err
}

// Fetch Method
func (r *CategoryRepository) Fetch(offset int, limit int) ([]dm.Category, error) {
	query := `
		SELECT id, category_name FROM categories OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	categories := make([]dm.Category, 0)

	for rows.Next() {
		category := dm.Category{}

		err = rows.Scan(
			&category.ID,
			&category.Name,
		)

		if err != nil {
			return nil, err
		}

		categories = append(categories, category)
	}

	return categories, nil
}

// GetByID Method
func (r *CategoryRepository) GetByID(id string) (dm.Category, error) {
	query := `
		SELECT id, category_name FROM categories WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	category := dm.Category{}

	err := row.Scan(
		&category.ID,
		&category.Name,
	)

	if err != nil {
		return dm.Category{}, err
	}

	return category, nil
}

// Update Method
func (r *CategoryRepository) Update(category dm.Category) error {
	query := `
		UPDATE categories SET category_name=$2 WHERE ID = $1
	`

	_, err := r.db.Query(query, category.ID, category.Name)

	if err != nil {
		return err
	}

	return nil
}

// Delete Method
func (r *CategoryRepository) Delete(id string) error {
	query := `
		DELETE FROM categories WHERE id = $1
	`

	_, err := r.db.Query(query, id)

	if err != nil {
		return err
	}

	return nil
}
