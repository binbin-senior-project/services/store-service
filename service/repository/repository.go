package repository

// RegisterRepository struct
type RegisterRepository struct {
	Analytic    IAnalyticRepository
	Category    ICategoryRepository
	Store       IStoreRepository
	Transaction ITransactionRepository
}
