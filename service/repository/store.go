package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
)

// IStoreRepository interface
type IStoreRepository interface {
	Store(store dm.Store) error
	Fetch(offset int32, limit int32) ([]dm.Store, error)
	GetByCategoryID(categoryID string, offset int32, limit int32) ([]dm.Store, error)
	GetByKeyword(keyword string, offset int32, limit int32) ([]dm.Store, error)
	GetByUserID(userID string, offset int32, limit int32) ([]dm.Store, error)
	GetByID(id string) (dm.Store, error)
	Update(store dm.Store) error
	UpdateByKey(id string, key string, value string) error
}

// StoreRepository is an object
type StoreRepository struct {
	db *sql.DB
}

// NewStoreRepository is an inital method
func NewStoreRepository(db *sql.DB) IStoreRepository {
	return &StoreRepository{
		db: db,
	}
}

// Store method
func (r *StoreRepository) Store(store dm.Store) error {
	query := `
		INSERT INTO stores (id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, user_id, category_id)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	store.ID = uuid.New().String()

	_, err := r.db.Query(
		query,
		store.ID,
		store.Logo,
		store.Name,
		store.Tagline,
		store.Phone,
		store.Latitude,
		store.Longitude,
		store.UserID,
		store.CategoryID,
	)

	return err
}

// Fetch method
func (r *StoreRepository) Fetch(offset int32, limit int32) ([]dm.Store, error) {
	query := `
		SELECT id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, store_status, user_id, category_id
		FROM stores ORDER BY created_at DESC OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	stores := make([]dm.Store, 0)

	for rows.Next() {
		store := dm.Store{}

		err = rows.Scan(
			&store.ID,
			&store.Logo,
			&store.Name,
			&store.Tagline,
			&store.Phone,
			&store.Latitude,
			&store.Longitude,
			&store.Status,
			&store.UserID,
			&store.CategoryID,
		)

		if err != nil {
			return nil, err
		}

		stores = append(stores, store)
	}

	return stores, nil
}

// GetByCategoryID method
func (r *StoreRepository) GetByCategoryID(categoryID string, offset int32, limit int32) ([]dm.Store, error) {
	query := `
		SELECT id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, store_status, user_id, category_id
		FROM stores WHERE category_id = $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, categoryID, offset, limit)

	if err != nil {
		return nil, err
	}

	stores := make([]dm.Store, 0)

	for rows.Next() {
		store := dm.Store{}

		err = rows.Scan(
			&store.ID,
			&store.Logo,
			&store.Name,
			&store.Tagline,
			&store.Phone,
			&store.Latitude,
			&store.Longitude,
			&store.Status,
			&store.UserID,
			&store.CategoryID,
		)

		if err != nil {
			return nil, err
		}

		stores = append(stores, store)
	}

	return stores, nil
}

// GetByKeyword method
func (r *StoreRepository) GetByKeyword(keyword string, offset int32, limit int32) ([]dm.Store, error) {
	query := `
		SELECT id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, store_status, user_id, category_id
		FROM stores WHERE store_name LIKE $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, "%"+keyword+"%", offset, limit)

	if err != nil {
		return nil, err
	}

	stores := make([]dm.Store, 0)

	for rows.Next() {
		store := dm.Store{}

		err = rows.Scan(
			&store.ID,
			&store.Logo,
			&store.Name,
			&store.Tagline,
			&store.Phone,
			&store.Latitude,
			&store.Longitude,
			&store.Status,
			&store.UserID,
			&store.CategoryID,
		)

		if err != nil {
			return nil, err
		}

		stores = append(stores, store)
	}

	return stores, nil
}

// GetByUserID method
func (r *StoreRepository) GetByUserID(userID string, offset int32, limit int32) ([]dm.Store, error) {
	query := `
		SELECT id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, store_status, user_id, category_id
		FROM stores WHERE user_id = $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, userID, offset, limit)

	if err != nil {
		return nil, err
	}

	stores := make([]dm.Store, 0)

	for rows.Next() {
		store := dm.Store{}

		err = rows.Scan(
			&store.ID,
			&store.Logo,
			&store.Name,
			&store.Tagline,
			&store.Phone,
			&store.Latitude,
			&store.Longitude,
			&store.Status,
			&store.UserID,
			&store.CategoryID,
		)

		if err != nil {
			return nil, err
		}

		stores = append(stores, store)
	}

	return stores, nil
}

// GetByID method
func (r *StoreRepository) GetByID(id string) (dm.Store, error) {
	query := `
		SELECT id, store_logo, store_name, store_tagline, store_phone, store_latitude, store_longitude, store_status, user_id, category_id
		FROM stores WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	store := dm.Store{}

	err := row.Scan(
		&store.ID,
		&store.Logo,
		&store.Name,
		&store.Tagline,
		&store.Phone,
		&store.Latitude,
		&store.Longitude,
		&store.Status,
		&store.UserID,
		&store.CategoryID,
	)

	if err != nil {
		return dm.Store{}, err
	}

	return store, nil
}

// Update method
func (r *StoreRepository) Update(store dm.Store) error {
	query := `
		UPDATE stores
		SET store_logo=$2, store_name=$3, store_tagline=$4, store_phone=$5, store_latitude=$6, store_longitude=$7, category_id=$8
		WHERE ID = $1
	`

	_, err := r.db.Query(
		query,
		store.ID,
		store.Logo,
		store.Name,
		store.Tagline,
		store.Phone,
		store.Latitude,
		store.Longitude,
		store.CategoryID,
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateByKey method
func (r *StoreRepository) UpdateByKey(id string, key string, value string) error {
	query := `UPDATE stores SET ` + key + `=$2 WHERE ID = $1`

	_, err := r.db.Query(query, id, value)

	if err != nil {
		return err
	}

	return nil
}
