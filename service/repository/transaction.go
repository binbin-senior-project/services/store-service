package repository

import (
	"database/sql"
	"time"

	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
)

// ITransactionRepository interface
type ITransactionRepository interface {
	Store(transaction dm.Transaction) error
	GetByStoreID(storeID string, offset int, limit int) ([]dm.Transaction, error)
	GetByID(transactionID string) (dm.Transaction, error)
}

// TransactionRepository struct
type TransactionRepository struct {
	db *sql.DB
}

// NewTransactionRepository method
func NewTransactionRepository(db *sql.DB) ITransactionRepository {
	return &TransactionRepository{
		db: db,
	}
}

// Store method
func (r *TransactionRepository) Store(transaction dm.Transaction) error {
	query := `
		INSERT INTO transactions (store_id, transaction_description, transaction_created_at)
		VALUES ($1, $2, $3)
	`

	_, err := r.db.Query(
		query,
		transaction.StoreID,
		transaction.Description,
		time.Now(),
	)

	return err
}

// GetByStoreID method
func (r *TransactionRepository) GetByStoreID(storeID string, offset int, limit int) ([]dm.Transaction, error) {
	query := `
		SELECT id, transaction_description, transaction_created_at
		FROM transactions
		WHERE store_id = $1
		ORDER BY transaction_created_at DESC
		OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, storeID, offset, limit)

	if err != nil {
		return nil, err
	}

	transactions := make([]dm.Transaction, 0)

	for rows.Next() {
		transaction := dm.Transaction{}

		err = rows.Scan(
			&transaction.ID,
			&transaction.Description,
			&transaction.CreatedAt,
		)

		if err != nil {
			return nil, err
		}

		transactions = append(transactions, transaction)
	}

	return transactions, nil
}

// GetByID method
func (r *TransactionRepository) GetByID(id string) (dm.Transaction, error) {
	query := `
		SELECT id, transaction_description, transaction_created_at
		FROM transactions WHERE ID = $1
	`

	row := r.db.QueryRow(query, id)

	transaction := dm.Transaction{}

	err := row.Scan(
		&transaction.ID,
		&transaction.Description,
		&transaction.CreatedAt,
	)

	if err != nil {
		return dm.Transaction{}, nil
	}

	return transaction, nil
}
