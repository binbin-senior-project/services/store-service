package usecase

import (
	"gitlab.com/mangbinbin/services/store-service/service/delivery/amqp"
	r "gitlab.com/mangbinbin/services/store-service/service/repository"
)

// IAnalyticUsecase interface
type IAnalyticUsecase interface {
	GetNewStoreToday() (int, error)
}

// AnalyticUsecase is an object
type AnalyticUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewAnalyticUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewAnalyticUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IAnalyticUsecase {
	return &AnalyticUsecase{
		repo:   repo,
		broker: broker,
	}
}

// GetNewStoreToday method
func (u *AnalyticUsecase) GetNewStoreToday() (int, error) {
	count, err := u.repo.Analytic.GetNewStoreToday()

	if err != nil {
		return 0, err
	}

	return count, nil
}
