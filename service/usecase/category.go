package usecase

import (
	"gitlab.com/mangbinbin/services/store-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
	r "gitlab.com/mangbinbin/services/store-service/service/repository"
)

// ICategoryUsecase interface
type ICategoryUsecase interface {
	Store(category dm.Category) error
	Fetch(offset int, limit int) ([]dm.Category, error)
	GetByID(id string) (dm.Category, error)
	Update(category dm.Category) error
	Delete(id string) error
}

// CategoryUsecase is an object
type CategoryUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewCategoryUsecase is an inital method
func NewCategoryUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) ICategoryUsecase {
	return &CategoryUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *CategoryUsecase) Store(category dm.Category) error {
	err := u.repo.Category.Store(category)

	return err
}

// Fetch method
func (u *CategoryUsecase) Fetch(offset int, limit int) ([]dm.Category, error) {
	categories, err := u.repo.Category.Fetch(offset, limit)

	if err != nil {
		return []dm.Category{}, err
	}

	return categories, nil
}

// GetByID method
func (u *CategoryUsecase) GetByID(id string) (dm.Category, error) {
	category, err := u.repo.Category.GetByID(id)

	if err != nil {
		return dm.Category{}, err
	}

	return category, nil
}

// Update method
func (u *CategoryUsecase) Update(category dm.Category) error {
	err := u.repo.Category.Update(category)

	if err != nil {
		return err
	}

	return nil
}

// Delete method
func (u *CategoryUsecase) Delete(id string) error {
	err := u.repo.Category.Delete(id)

	if err != nil {
		return err
	}

	return nil
}
