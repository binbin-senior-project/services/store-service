package usecase

import (
	"gitlab.com/mangbinbin/services/store-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
	r "gitlab.com/mangbinbin/services/store-service/service/repository"
)

// IStoreUsecase interface
type IStoreUsecase interface {
	Store(store dm.Store) error
	Fetch(offset int32, limit int32) ([]dm.Store, error)
	GetByCategoryID(categoryID string, offset int32, limit int32) ([]dm.Store, error)
	GetByKeyword(keyword string, offset int32, limit int32) ([]dm.Store, error)
	GetByUserID(userID string, offset int32, limit int32) ([]dm.Store, error)
	GetByID(id string) (dm.Store, error)
	Update(store dm.Store) error
	Activate(id string) error
	Ban(id string) error
}

// StoreUsecase is an object
type StoreUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewStoreUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewStoreUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IStoreUsecase {
	return &StoreUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *StoreUsecase) Store(store dm.Store) error {
	err := u.repo.Store.Store(store)

	return err
}

// Fetch method
func (u *StoreUsecase) Fetch(offset int32, limit int32) ([]dm.Store, error) {
	stores, err := u.repo.Store.Fetch(offset, limit)

	if err != nil {
		return []dm.Store{}, err
	}

	return stores, nil
}

// GetByCategoryID method
func (u *StoreUsecase) GetByCategoryID(categoryID string, offset int32, limit int32) ([]dm.Store, error) {
	stores, err := u.repo.Store.GetByCategoryID(categoryID, offset, limit)

	if err != nil {
		return []dm.Store{}, err
	}

	return stores, nil
}

// GetByKeyword method
func (u *StoreUsecase) GetByKeyword(keyword string, offset int32, limit int32) ([]dm.Store, error) {
	stores, err := u.repo.Store.GetByKeyword(keyword, offset, limit)

	if err != nil {
		return []dm.Store{}, err
	}

	return stores, nil
}

// GetByUserID method
func (u *StoreUsecase) GetByUserID(userID string, offset int32, limit int32) ([]dm.Store, error) {
	stores, err := u.repo.Store.GetByUserID(userID, offset, limit)

	if err != nil {
		return []dm.Store{}, err
	}

	return stores, nil
}

// GetByID method
func (u *StoreUsecase) GetByID(id string) (dm.Store, error) {
	store, err := u.repo.Store.GetByID(id)

	if err != nil {
		return dm.Store{}, err
	}

	return store, nil
}

// Update method
func (u *StoreUsecase) Update(store dm.Store) error {
	err := u.repo.Store.Update(store)

	if err != nil {
		return err
	}

	return nil
}

// Activate method
func (u *StoreUsecase) Activate(id string) error {
	err := u.repo.Store.UpdateByKey(id, "store_status", "active")

	if err != nil {
		return err
	}

	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: id,
		Title:  "แจ้งเตือนบัญชี",
		Body:   "ร้านค้าของคุณได้รับการอนุมัติการใช้งาน",
	})

	return nil
}

// Ban method
func (u *StoreUsecase) Ban(id string) error {
	err := u.repo.Store.UpdateByKey(id, "store_status", "banned")

	if err != nil {
		return err
	}

	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: id,
		Title:  "แจ้งเตือนบัญชี",
		Body:   "ร้านค้าของคุณโดนระงับการใช้งาน",
	})

	return nil
}
