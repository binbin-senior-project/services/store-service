package usecase

import (
	"gitlab.com/mangbinbin/services/store-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/store-service/service/domain"
	r "gitlab.com/mangbinbin/services/store-service/service/repository"
)

// ITransactionUsecase interface
type ITransactionUsecase interface {
	Store(transaction dm.Transaction) error
	GetByStoreID(userID string, offset int, limit int) ([]dm.Transaction, error)
	GetByID(transactionID string) (dm.Transaction, error)
}

// TransactionUsecase is a struct
type TransactionUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewTransactionUsecase method
func NewTransactionUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) ITransactionUsecase {
	return &TransactionUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *TransactionUsecase) Store(transaction dm.Transaction) error {
	err := u.repo.Transaction.Store(transaction)

	if err != nil {
		return err
	}

	return nil
}

// GetByStoreID method
func (u *TransactionUsecase) GetByStoreID(userID string, offset int, limit int) ([]dm.Transaction, error) {
	transactions, err := u.repo.Transaction.GetByStoreID(userID, offset, limit)

	if err != nil {
		return []dm.Transaction{}, err
	}

	return transactions, nil
}

// GetByID method
func (u *TransactionUsecase) GetByID(id string) (dm.Transaction, error) {
	transaction, err := u.repo.Transaction.GetByID(id)

	if err != nil {
		return dm.Transaction{}, err
	}

	return transaction, nil
}
