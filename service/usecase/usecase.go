package usecase

// RegisterUsecase struct
type RegisterUsecase struct {
	Analytic    IAnalyticUsecase
	Category    ICategoryUsecase
	Store       IStoreUsecase
	Transaction ITransactionUsecase
}
